package com.example;

import com.example.mapper.OaStaffMapper;
import com.example.model.OaStaff;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class OaStaffTableTest {
    private static final Logger log = LoggerFactory.getLogger(OaStaffTableTest.class);
    private SqlSessionFactory sqlSessionFactory;
    private SqlSession sqlSession;
    private OaStaffMapper oaStaffMapper;
    @Before
    public void init() throws IOException {
        String resource = "MapperConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
        oaStaffMapper = sqlSession.getMapper(OaStaffMapper.class);
    }

    @Test
    public void testInsert() {
        OaStaff staff = new OaStaff();
        staff.setName("赵武");
        staff.setAddress("月球");
        staff.setGender(Byte.valueOf("1"));
        Date now = new Date();
        staff.setGmtCreate(now);
        staff.setGmtModified(now);
        oaStaffMapper.insert(staff);
        sqlSession.commit();
    }

    @Test
    public void testSelect() {
        OaStaff staff = oaStaffMapper.selectByPrimaryKey(4L);
        log.info("id = {}, name={} addr= {}, create= {}", staff.getId(), staff.getName(), staff.getAddress(), staff.getGmtCreate());

    }

    @Test
    public void testUpdate() {
        OaStaff staff = oaStaffMapper.selectByPrimaryKey(4L);
        Date now = new Date();
        staff.setGmtModified(now);
        oaStaffMapper.updateByPrimaryKey(staff);
    }

    @Test
    public void testDelete() {
        oaStaffMapper.deleteByPrimaryKey(390L);
    }
    @After
    public void destroy() {
        sqlSession.close();
    }

}
