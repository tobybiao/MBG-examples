package com.example;

import com.example.mapper.OaStaffMapper;
import com.example.model.OaStaff;
import com.example.model.OaStaffExample;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

public class OaStaffTableTest {
    static final Logger log = LoggerFactory.getLogger(OaStaffTableTest.class);
    SqlSessionFactory sqlSessionFactory;
    SqlSession sqlSession;
    OaStaffMapper oaStaffMapper;
    @Before
    public void init() throws IOException {
        String resource = "MapperConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
        oaStaffMapper = sqlSession.getMapper(OaStaffMapper.class);
    }

    @Test
    public void testInsert() {
        OaStaff staff = new OaStaff();
        staff.setName("张三");
        staff.setAddress("火星");
        staff.setGender(new Byte("1"));
        Date now = new Date();
        staff.setGmtCreate(now);
        staff.setGmtModified(now);
        oaStaffMapper.insert(staff);
        sqlSession.commit();
    }

    @Test
    public void testSelect() {
        OaStaff staff = oaStaffMapper.selectByPrimaryKey(3L);
        log.info("id = {}, name={} addr= {}, create= {}", staff.getId(), staff.getName(), staff.getAddress(), staff.getGmtCreate());
        OaStaffExample example = new OaStaffExample();
        example.or()
                .andNameLike("l%");
        List<OaStaff> oaStaffs = oaStaffMapper.selectByExample(example);
        for (OaStaff s : oaStaffs) {
            log.info("id = {}, name={} addr= {}, create= {}", s.getId(), s.getName(), s.getAddress(), s.getGmtCreate());
        }
    }

    @Test
    public void testUpdate() {
        OaStaff staff = oaStaffMapper.selectByPrimaryKey(3L);
        Date now = new Date();
        staff.setGmtModified(now);
//        oaStaffMapper.updateByPrimaryKey(staff); // ok

        OaStaffExample example = new OaStaffExample();
        example.or()
                .andNameLike("l%");
        OaStaff s = new OaStaff();
        s.setAddress("第七区");
//        oaStaffMapper.updateByExample(s, example); // error! 所有字段全部更新，而s.id为空，主键不允许为空导致更新失败
        oaStaffMapper.updateByExampleSelective(s, example); // ok 只更更新 s 中不为空的字段
    }

    @Test
    public void testDelete() {
        oaStaffMapper.deleteByPrimaryKey(390L);
        OaStaffExample example = new OaStaffExample();
        example.or()
                .andNameLike("l%");
        oaStaffMapper.deleteByExample(example);
    }
    @After
    public void destroy() {
        sqlSession.close();
    }

}
