package com.example;

import com.example.mapper.OaStaffMapper;
import com.example.model.OaStaff;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import static com.example.mapper.OaStaffDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

public class OaStaffTableTest {
    private final Logger log = LoggerFactory.getLogger(OaStaffTableTest.class);
    static SqlSessionFactory sqlSessionFactory;
    private SqlSession sqlSession;
    private OaStaffMapper oaStaffMapper;

    @Before
    public void init() throws IOException {
        String resource = "MapperConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
        oaStaffMapper = sqlSession.getMapper(OaStaffMapper.class);
    }

    @Test
    public void testInsert() {
        OaStaff oaStaff = new OaStaff();
        oaStaff.setName("lisi");
        oaStaff.setGender(Byte.parseByte("0"));
        oaStaffMapper.insert(oaStaff);
        sqlSession.commit();
    }

    @Test
    public void testSelect() {
        Optional<OaStaff> oaStaffOptional = oaStaffMapper.selectByPrimaryKey(1L);
        OaStaff staff = oaStaffOptional.get();
        log.info("id = {}, name= {}, addr = {}", staff.getId(), staff.getName(), staff.getAddress());
        List<OaStaff> select = oaStaffMapper.select(c -> c.where(id, isEqualTo(1L)));
        for (OaStaff s : select) {
            log.info("name={}, id = {}, addr = {}", s.getName(), s.getId(), s.getAddress());
        }
    }

    @Test
    public void testUpdate() {
        Optional<OaStaff> oaStaffOptional = oaStaffMapper.selectByPrimaryKey(1L);
        OaStaff oaStaff = oaStaffOptional.get();
        oaStaff.setAddress("北京市海淀区");
        oaStaffMapper.updateByPrimaryKey(oaStaff);
        oaStaffMapper.update(c -> c.set(address).equalTo("北京市朝阳区")
                .set(gender).equalTo(Byte.parseByte("1"))
                .where(id, isEqualTo(2L)));
        sqlSession.commit();
    }

    @Test
    public void testDelete() {
        oaStaffMapper.deleteByPrimaryKey(40L);
        oaStaffMapper.delete(c -> c.where(name, isEqualTo("libiao"), and(gender, isEqualTo(Byte.parseByte("1")))).or(address, isEqualTo("广州市")));
    }
    @After
    public void destroy() {
        sqlSession.close();
    }

}
