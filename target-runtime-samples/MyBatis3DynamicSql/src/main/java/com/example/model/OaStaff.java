package com.example.model;

import java.util.Date;
import javax.annotation.Generated;

public class OaStaff {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.044+08:00", comments="Source field: oa_staff.id")
    private Long id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.name")
    private String name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.gender")
    private Byte gender;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.birthday")
    private Date birthday;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.address")
    private String address;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.native_place")
    private String nativePlace;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.hiredate")
    private Date hiredate;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.gmt_create")
    private Date gmtCreate;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.048+08:00", comments="Source field: oa_staff.gmt_modified")
    private Date gmtModified;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.046+08:00", comments="Source field: oa_staff.id")
    public Long getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.046+08:00", comments="Source field: oa_staff.id")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.name")
    public String getName() {
        return name;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.name")
    public void setName(String name) {
        this.name = name;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.gender")
    public Byte getGender() {
        return gender;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.gender")
    public void setGender(Byte gender) {
        this.gender = gender;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.birthday")
    public Date getBirthday() {
        return birthday;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.birthday")
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.address")
    public String getAddress() {
        return address;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.address")
    public void setAddress(String address) {
        this.address = address;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.native_place")
    public String getNativePlace() {
        return nativePlace;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.native_place")
    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.hiredate")
    public Date getHiredate() {
        return hiredate;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.hiredate")
    public void setHiredate(Date hiredate) {
        this.hiredate = hiredate;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.gmt_create")
    public Date getGmtCreate() {
        return gmtCreate;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.047+08:00", comments="Source field: oa_staff.gmt_create")
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.048+08:00", comments="Source field: oa_staff.gmt_modified")
    public Date getGmtModified() {
        return gmtModified;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.048+08:00", comments="Source field: oa_staff.gmt_modified")
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}