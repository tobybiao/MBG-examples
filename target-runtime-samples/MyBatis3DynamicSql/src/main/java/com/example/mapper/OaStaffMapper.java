package com.example.mapper;

import static com.example.mapper.OaStaffDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;

import com.example.model.OaStaff;
import java.util.List;
import java.util.Optional;
import javax.annotation.Generated;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;

@Mapper
public interface OaStaffMapper {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.06+08:00", comments="Source Table: oa_staff")
    BasicColumn[] selectList = BasicColumn.columnList(id, name, gender, birthday, address, nativePlace, hiredate, gmtCreate, gmtModified);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.052+08:00", comments="Source Table: oa_staff")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    long count(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.053+08:00", comments="Source Table: oa_staff")
    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
    int delete(DeleteStatementProvider deleteStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.053+08:00", comments="Source Table: oa_staff")
    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="record.id", before=true, resultType=Long.class)
    int insert(InsertStatementProvider<OaStaff> insertStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.056+08:00", comments="Source Table: oa_staff")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("OaStaffResult")
    Optional<OaStaff> selectOne(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.056+08:00", comments="Source Table: oa_staff")
    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="OaStaffResult", value = {
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.TINYINT),
        @Result(column="birthday", property="birthday", jdbcType=JdbcType.DATE),
        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
        @Result(column="native_place", property="nativePlace", jdbcType=JdbcType.VARCHAR),
        @Result(column="hiredate", property="hiredate", jdbcType=JdbcType.DATE),
        @Result(column="gmt_create", property="gmtCreate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="gmt_modified", property="gmtModified", jdbcType=JdbcType.TIMESTAMP)
    })
    List<OaStaff> selectMany(SelectStatementProvider selectStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.057+08:00", comments="Source Table: oa_staff")
    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
    int update(UpdateStatementProvider updateStatement);

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.057+08:00", comments="Source Table: oa_staff")
    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, oaStaff, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.057+08:00", comments="Source Table: oa_staff")
    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, oaStaff, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.058+08:00", comments="Source Table: oa_staff")
    default int deleteByPrimaryKey(Long id_) {
        return delete(c -> 
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.058+08:00", comments="Source Table: oa_staff")
    default int insert(OaStaff record) {
        return MyBatis3Utils.insert(this::insert, record, oaStaff, c ->
            c.map(id).toProperty("id")
            .map(name).toProperty("name")
            .map(gender).toProperty("gender")
            .map(birthday).toProperty("birthday")
            .map(address).toProperty("address")
            .map(nativePlace).toProperty("nativePlace")
            .map(hiredate).toProperty("hiredate")
            .map(gmtCreate).toProperty("gmtCreate")
            .map(gmtModified).toProperty("gmtModified")
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.059+08:00", comments="Source Table: oa_staff")
    default int insertSelective(OaStaff record) {
        return MyBatis3Utils.insert(this::insert, record, oaStaff, c ->
            c.map(id).toProperty("id")
            .map(name).toPropertyWhenPresent("name", record::getName)
            .map(gender).toPropertyWhenPresent("gender", record::getGender)
            .map(birthday).toPropertyWhenPresent("birthday", record::getBirthday)
            .map(address).toPropertyWhenPresent("address", record::getAddress)
            .map(nativePlace).toPropertyWhenPresent("nativePlace", record::getNativePlace)
            .map(hiredate).toPropertyWhenPresent("hiredate", record::getHiredate)
            .map(gmtCreate).toPropertyWhenPresent("gmtCreate", record::getGmtCreate)
            .map(gmtModified).toPropertyWhenPresent("gmtModified", record::getGmtModified)
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.061+08:00", comments="Source Table: oa_staff")
    default Optional<OaStaff> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, oaStaff, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.061+08:00", comments="Source Table: oa_staff")
    default List<OaStaff> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, oaStaff, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.061+08:00", comments="Source Table: oa_staff")
    default List<OaStaff> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, oaStaff, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.062+08:00", comments="Source Table: oa_staff")
    default Optional<OaStaff> selectByPrimaryKey(Long id_) {
        return selectOne(c ->
            c.where(id, isEqualTo(id_))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.062+08:00", comments="Source Table: oa_staff")
    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, oaStaff, completer);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.062+08:00", comments="Source Table: oa_staff")
    static UpdateDSL<UpdateModel> updateAllColumns(OaStaff record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalTo(record::getId)
                .set(name).equalTo(record::getName)
                .set(gender).equalTo(record::getGender)
                .set(birthday).equalTo(record::getBirthday)
                .set(address).equalTo(record::getAddress)
                .set(nativePlace).equalTo(record::getNativePlace)
                .set(hiredate).equalTo(record::getHiredate)
                .set(gmtCreate).equalTo(record::getGmtCreate)
                .set(gmtModified).equalTo(record::getGmtModified);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.063+08:00", comments="Source Table: oa_staff")
    static UpdateDSL<UpdateModel> updateSelectiveColumns(OaStaff record, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(id).equalToWhenPresent(record::getId)
                .set(name).equalToWhenPresent(record::getName)
                .set(gender).equalToWhenPresent(record::getGender)
                .set(birthday).equalToWhenPresent(record::getBirthday)
                .set(address).equalToWhenPresent(record::getAddress)
                .set(nativePlace).equalToWhenPresent(record::getNativePlace)
                .set(hiredate).equalToWhenPresent(record::getHiredate)
                .set(gmtCreate).equalToWhenPresent(record::getGmtCreate)
                .set(gmtModified).equalToWhenPresent(record::getGmtModified);
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.064+08:00", comments="Source Table: oa_staff")
    default int updateByPrimaryKey(OaStaff record) {
        return update(c ->
            c.set(name).equalTo(record::getName)
            .set(gender).equalTo(record::getGender)
            .set(birthday).equalTo(record::getBirthday)
            .set(address).equalTo(record::getAddress)
            .set(nativePlace).equalTo(record::getNativePlace)
            .set(hiredate).equalTo(record::getHiredate)
            .set(gmtCreate).equalTo(record::getGmtCreate)
            .set(gmtModified).equalTo(record::getGmtModified)
            .where(id, isEqualTo(record::getId))
        );
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.064+08:00", comments="Source Table: oa_staff")
    default int updateByPrimaryKeySelective(OaStaff record) {
        return update(c ->
            c.set(name).equalToWhenPresent(record::getName)
            .set(gender).equalToWhenPresent(record::getGender)
            .set(birthday).equalToWhenPresent(record::getBirthday)
            .set(address).equalToWhenPresent(record::getAddress)
            .set(nativePlace).equalToWhenPresent(record::getNativePlace)
            .set(hiredate).equalToWhenPresent(record::getHiredate)
            .set(gmtCreate).equalToWhenPresent(record::getGmtCreate)
            .set(gmtModified).equalToWhenPresent(record::getGmtModified)
            .where(id, isEqualTo(record::getId))
        );
    }
}