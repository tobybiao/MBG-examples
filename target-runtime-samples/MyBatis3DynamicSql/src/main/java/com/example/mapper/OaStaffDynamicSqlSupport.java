package com.example.mapper;

import java.sql.JDBCType;
import java.util.Date;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class OaStaffDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.05+08:00", comments="Source Table: oa_staff")
    public static final OaStaff oaStaff = new OaStaff();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source field: oa_staff.id")
    public static final SqlColumn<Long> id = oaStaff.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source field: oa_staff.name")
    public static final SqlColumn<String> name = oaStaff.name;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source field: oa_staff.gender")
    public static final SqlColumn<Byte> gender = oaStaff.gender;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source field: oa_staff.birthday")
    public static final SqlColumn<Date> birthday = oaStaff.birthday;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source field: oa_staff.address")
    public static final SqlColumn<String> address = oaStaff.address;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source field: oa_staff.native_place")
    public static final SqlColumn<String> nativePlace = oaStaff.nativePlace;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.052+08:00", comments="Source field: oa_staff.hiredate")
    public static final SqlColumn<Date> hiredate = oaStaff.hiredate;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.052+08:00", comments="Source field: oa_staff.gmt_create")
    public static final SqlColumn<Date> gmtCreate = oaStaff.gmtCreate;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.052+08:00", comments="Source field: oa_staff.gmt_modified")
    public static final SqlColumn<Date> gmtModified = oaStaff.gmtModified;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2020-05-03T21:35:35.051+08:00", comments="Source Table: oa_staff")
    public static final class OaStaff extends SqlTable {
        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);

        public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

        public final SqlColumn<Byte> gender = column("gender", JDBCType.TINYINT);

        public final SqlColumn<Date> birthday = column("birthday", JDBCType.DATE);

        public final SqlColumn<String> address = column("address", JDBCType.VARCHAR);

        public final SqlColumn<String> nativePlace = column("native_place", JDBCType.VARCHAR);

        public final SqlColumn<Date> hiredate = column("hiredate", JDBCType.DATE);

        public final SqlColumn<Date> gmtCreate = column("gmt_create", JDBCType.TIMESTAMP);

        public final SqlColumn<Date> gmtModified = column("gmt_modified", JDBCType.TIMESTAMP);

        public OaStaff() {
            super("oa_staff");
        }
    }
}