# 通过Maven plugin运行
## 准备数据表
两张表

`oa_staff`
```sql
CREATE TABLE `oa_staff` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '名字',
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '性别：0->男，1->女',
  `birthday` date DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL COMMENT '住址',
  `native_place` varchar(80) DEFAULT NULL COMMENT '籍贯',
  `hiredate` date DEFAULT NULL COMMENT '入职日期',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='员工信息'
```

`oa_user`
```sql
CREATE TABLE `oa_user` (
  `id` bigint(20) unsigned NOT NULL,
  `username` varchar(16) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='oa系统用户'
```

## pom.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>mbg-demo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <properties>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.8</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.19</version>
            <scope>runtime</scope>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.mybatis.generator</groupId>
                <artifactId>mybatis-generator-maven-plugin</artifactId>
                <version>1.4.0</version>
                <configuration>
                    <includeAllDependencies>true</includeAllDependencies>
                    <verbose>true</verbose>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

## generatorConfig.xml

`src\main\resources\generatorConfig.xml`
```xml
<!DOCTYPE generatorConfiguration PUBLIC
        "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
<generatorConfiguration>
    <context id="simple" targetRuntime="MyBatis3Simple">
        <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://192.168.56.101:3306/db_example?characterEncoding=utf8&amp;autoReconnect=true&amp;serverTimezone=PRC"
                        userId="root" password="123456"/>

        <javaModelGenerator targetPackage="com.example.model" targetProject="src/main/java"/>

        <sqlMapGenerator targetPackage="com.example.mapper" targetProject="src/main/resources"/>

        <javaClientGenerator type="XMLMAPPER" targetPackage="com.example.mapper" targetProject="src/main/java"/>

        <table tableName="oa_staff">
            <generatedKey column="id" sqlStatement="MySql" />
        </table>
        <table tableName="oa_user" />
    </context>
</generatorConfiguration>
```
配置文件名默认是`${basedir}/src/main/resources/generatorConfig.xml`，可以通过`configurationFile`指定别的名字
## 运行MyBatis Generator
有两种方式：
- 通过命令行
  ```shell
  mvn mybatis-generator:generate
  ```
- Idea图形界面鼠标点击

  ![maven-plugin](img/maven-plugin.png)
## 重复运行
xml文件会自动合并，保留自定义修改

但是Java文件不会自动合并，产生新的文件，文件名例如：`OaStaffMapper.java.1`、`OaStaffMapper.java.2`
## 参考
> [Running MyBatis Generator With Maven](http://mybatis.org/generator/running/runningWithMaven.html)