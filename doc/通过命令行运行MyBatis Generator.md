# 通过命令行运行MyBatis Generator
## 准备数据表
两张表

`oa_staff`
```sql
CREATE TABLE `oa_staff` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '名字',
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '性别：0->男，1->女',
  `birthday` date DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL COMMENT '住址',
  `native_place` varchar(80) DEFAULT NULL COMMENT '籍贯',
  `hiredate` date DEFAULT NULL COMMENT '入职日期',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='员工信息'
```

`oa_user`
```sql
CREATE TABLE `oa_user` (
  `id` bigint(20) unsigned NOT NULL,
  `username` varchar(16) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='oa系统用户'
```

## 下载依赖的jar包
可以去 以下网站搜索`mybatis-generator-core`、`mysql-connector-java`

https://mvnrepository.com/

https://mvnrepository.com/

比如：
下载地址：
```
https://repo1.maven.org/maven2/org/mybatis/generator/mybatis-generator-core/1.4.0/mybatis-generator-core-1.4.0.jar

https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.19/mysql-connector-java-8.0.19.jar
```

## 准备配置文件

```xml
<!DOCTYPE generatorConfiguration PUBLIC
        "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
<generatorConfiguration>
    <context id="simple" targetRuntime="MyBatis3Simple">
        <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://192.168.56.101:3306/db_example?characterEncoding=utf8&amp;autoReconnect=true&amp;serverTimezone=PRC"
                        userId="root" password="123456"/>

        <javaModelGenerator targetPackage="com.example.model" targetProject="generate-files/java"/>

        <sqlMapGenerator targetPackage="com.example.mapper" targetProject="generate-files/xml"/>

        <javaClientGenerator type="XMLMAPPER" targetPackage="com.example.mapper" targetProject="generate-files/java"/>

        <table tableName="oa_staff">
            <generatedKey column="id" sqlStatement="MySql" />
        </table>
        <table tableName="oa_user" />
    </context>
</generatorConfiguration>
```

## 运行生成

其中`targetProject` 指定目录必须先创建，否则生成失败。

运行前，目录结构如下：
```
E:\***\MBG-command>tree /F
卷 新加卷 的文件夹 PATH 列表
卷序列号为 5248-2E8E
E:.
│  generatorConfig.xml
│  mybatis-generator-core-1.4.0.jar
│  mysql-connector-java-8.0.19.jar
│
└─generate-files
    ├─java
    └─xml
```

执行命令：
```powershell
java -cp mybatis-generator-core-1.4.0.jar;mysql-connector-java-8.0.19.jar org.mybatis.generator.api.ShellRunner -configfile generatorConfig.xml
```

生成目录结构：
```
E:\***\MBG-command>tree /F
卷 新加卷 的文件夹 PATH 列表
卷序列号为 5248-2E8E
E:.
│  generatorConfig.xml
│  mybatis-generator-core-1.4.0.jar
│  mysql-connector-java-8.0.19.jar
│
└─generate-files
    ├─java
    │  └─com
    │      └─example
    │          ├─mapper
    │          │      OaStaffMapper.java
    │          │      OaUserMapper.java
    │          │
    │          └─model
    │                  OaStaff.java
    │                  OaUser.java
    │
    └─xml
        └─com
            └─example
                └─mapper
                        OaStaffMapper.xml
                        OaUserMapper.xml
```

