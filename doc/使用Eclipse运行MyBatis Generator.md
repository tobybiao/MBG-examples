# 使用Eclipse运行MyBatis Generator
## 准备数据表
两张表

`oa_staff`
```sql
CREATE TABLE `oa_staff` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '名字',
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '性别：0->男，1->女',
  `birthday` date DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL COMMENT '住址',
  `native_place` varchar(80) DEFAULT NULL COMMENT '籍贯',
  `hiredate` date DEFAULT NULL COMMENT '入职日期',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='员工信息'
```

`oa_user`
```sql
CREATE TABLE `oa_user` (
  `id` bigint(20) unsigned NOT NULL,
  `username` varchar(16) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='oa系统用户'
```

## 安装Eclipse功能
在Eclipse市场中搜索“ MyBatis Generator”并直接从市场中安装:

在菜单栏上点击 `Help`->`Eclipse Marketplace...`
![搜索安装](img/eclipse-feature.png)

点击`install` 开始安装，完成后同意协议并重启eclipse

## 使用Eclipse功能
1、先创建一个项目，这里创建Maven项目

pom.xml
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.exmple</groupId>
	<artifactId>mbg-demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<properties>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<java.version>1.8</java.version>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
	</properties>
	<dependencies>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>8.0.19</version>
			<scope>runtime</scope>
		</dependency>
	</dependencies>
</project>
```

2、添加生成配置

2.1 在Eclipse中依次点击File>New>Other...

2.2 从"MyBatis"分类下选择"MyBatis Generator Configuration File"，然后点击 "Next"

2.3 指定配置文件的存放路径和名字，默认文件名是`generatorConfig.xml`

![MyBatis-Generator-Configuration-File](img/MyBatis-Generator-Configuration-File.png)
![配置存放路径](img/eclipse-gen-config-location.png)

`generatorConfig.xml`内容为
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN" "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
<generatorConfiguration>
    <context id="simple" targetRuntime="MyBatis3Simple">
        <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://192.168.56.101:3306/db_example?characterEncoding=utf8&amp;autoReconnect=true&amp;serverTimezone=PRC"
                        userId="root" password="123456"/>

        <javaModelGenerator targetPackage="com.example.model" targetProject="mbg-demo"/>

        <sqlMapGenerator targetPackage="com.example.mapper" targetProject="mbg-demo"/>

        <javaClientGenerator type="XMLMAPPER" targetPackage="com.example.mapper" targetProject="mbg-demo"/>

        <table tableName="oa_staff">
            <generatedKey column="id" sqlStatement="MySql" />
        </table>
        <table tableName="oa_user" />
    </context>
</generatorConfiguration>
```

其中`targetProject`为eclipse项目名称，这里为Maven项目的GAV里的`artifactId`值**mbg-demo**，跟其他方式填写路径的方式不一样，这点需要注意，否则，会报项目不存在异常

3、运行

右键配置文件`generatorConfig.xml`，选择"Run As>Run MyBatis Generator"

## 重复运行
可以自动合并Java和xml文件，保留自定义修改
## 参考
[Running MyBatis Generator with Eclipse
Installing the Eclipse Feature](http://mybatis.org/generator/running/runningWithEclipse.html)
