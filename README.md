# MyBatis Generator
> 文中内容翻译自官方文档
## MyBatis Generator简介
MyBatis生成器（MBG）是MyBatis的代码生成器。它可以为所有版本的MyBatis生成代码。它检查一个数据库表（或多个表），并生成可用于访问表的工件。这减少了设置对象和配置文件以与数据库表交互的最初麻烦。MBG试图对大部分简单CRUD（插入、查找、更新、删除）的数据库操作产生重大影响。您仍然需要手工编写SQL和对象以用于连接查询或存储过程。
## MyBatis Generator 生成的工件
MBG根据配置方式，为不同的语言生成不同样式的代码。例如，MBG可以生成Java或Kotlin代码。而且MBG可以生成与MyBatis3兼容的XML—尽管现在它被认为是MBG的遗留使用。新样式的生成代码不需要XML。

根据配置方式，MyBatis生成器可能会生成：
- 与表结构匹配的Java或Kotlin类。这可能包括：
  - 与表主键匹配的类（如果有主键）
  - 与表的非主键字段匹配的类（BLOB字段除外）
  - 包含表的BLOB字段的类（如果表有BLOB字段）
  - 用于启用动态选择、更新和删除的类
  
  这些类之间存在适当的继承关系。注意，生成器可以配置为生成不同类型的POJO层次结构-例如，如果您愿意，可以选择为每个表生成一个域对象。
- 在某些情况下，MBG将生成与MyBatis3兼容的SQL映射XML文件。MBG为配置中的每个表上的简单CRUD方法生成SQL。生成的SQL语句包括：
  - 插入
  - 按主键更新
  - 按`example` 更新（使用动态where子句）
  - 按主键删除
  - 通过`example`删除（使用动态where子句）
  - 按主键查询
  - 按`example`查询（使用动态where子句）
  - 按`example`统计计数

- Java或Kotlin客户端类、接口和Kotlin扩展方法，它们适当地使用了上述对象。客户端类的生成是可选的。MBG将生成一个与MyBatis 3.x映射器基础设施一起工作的映射器接口
  
## 重复运行MyBatis Generator需要注意的问题
在迭代开发环境中，MyBatis生成器依然被设计得运行良好，它可以作为Ant任务或Maven插件包含在持续构建环境中。迭代运行MBG时需要注意的重要事项包括：
1. 如果存在与新生成的XML文件同名的现有文件，MBG将自动合并XML文件。MBG不会覆盖您对它生成的XML文件所做的任何自定义更改。您可以一遍又一遍地运行它，而不必担心丢失对XML的自定义更改。MBG将替换在上一次运行中生成的任何XML元素。
2. MBG不会合并Java文件，但它可以覆盖现有文件或用不同的唯一名称保存新生成的文件。如果对生成的Java文件进行更改并迭代运行MBG，则必须手动合并更改。当作为Eclipse插件运行时，MBG可以自动合并Java文件。
3. MBG不会合并Kotlin文件，它可以覆盖现有文件或用不同的唯一名称保存新生成的文件。如果对生成的Kotlin文件进行更改并迭代运行MBG，则必须手动合并更改。
   
## 依赖
MBG除了JRE之外没有依赖项。需要Java 8或更高版本。此外，还需要一个实现DatabaseMetaData接口的JDBC驱动程序，特别是getColumns和getPrimaryKeys方法。
## 运行MyBatis Generator的方式
MyBatis生成器（MBG）可以通过以下方式运行：
- 从带有XML配置的命令行终端
- 作为一个带有XML配置的Ant任务
- 作为Maven插件
- 作为另一个具有XML配置的Java程序
- 作为另一个基于Java的配置的Java程序
- 作为Eclipse特性

> 大部分内容翻译自

> http://mybatis.org/generator/index.html